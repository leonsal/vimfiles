"------------------------------------------------------------------------------
" VIM Configuraton file (NON GUI)
"------------------------------------------------------------------------------
"
"-----------------------------------------------------------------------------
" Common Settings
"-----------------------------------------------------------------------------

" Use Vim advanced features
set nocompatible
" Copy indent from previous line
set autoindent
" Set number of tabstops
set tabstop=4
" Number of spaces to use for each step of (auto)indent
set shiftwidth=4
" Number of spaces tha a Tab counts
set softtabstop=0
" Use the appropriate number of spaces to insert a Tab
set expandtab
" Number of columns at left to indicate open and close folds
set foldcolumn=0
" Incremental search
set noincsearch
" Highlight search hits
set nohlsearch
" Show command in status line
set showcmd
" Number of screen lines to use for command line
set cmdheight=1
" Always show status line
set laststatus=2
" Backspace over everything
set backspace=indent,eol,start
" Save at most 50 commands
set history=50
" Show the line and column of the cursor position
set ruler
" When a bracket is inserted, briefly jump to the matching one
set showmatch
" Time in tenths of a second to show the matching bracket
set matchtime=2
" No file backups
set nobackup
" Flash display instead of beeping
set novisualbell
" Enable mouse in normal and visual modes
set mouse=nv
" Viminfo file options
set viminfo=!,'100,f1,rE:,:100,/100,%
" When set the screen will not be redrawn while executing macros
set lazyredraw
" Don't make noise
set noerrorbells
" Automatically reread externally modified files
set autoread
" No file backups
set backupcopy=no
" Diff options
set diffopt=filler,iwhite
" Keep cursor column when moving
set nostartofline
" Encoding used for the terminal
set termencoding=utf-8
" Preferred help languages
set helplang=en
" Background color
set background=dark
" Show line numbers
set number
" Abandoned buffers are hidden
set hidden
" Do not wrap lines
set nowrap
" File encondings
set fileencodings=utf-8,default,latin1
" Language syntax highlight on
syntax on
" Select color scheme
colorscheme desert
" Set status line
set statusline=%<%F\ %w%r%y[%{&ff}]%m[%o]\%=\ %l,%v\ \ %p%%\ %L
" Set cursor line
set cursorline
" Set directory where swap files are stored
if has("win32")
	set directory=C:\Users\leonel\tmp
	set backupdir=C:\Users\leonel\tmp
else
	set directory=/var/tmp
endif
" Folding configuration
" Use zM to fold all, zR to unfold all
" zo to open one fold, zc to close one
set foldmethod=indent
set foldnestmax=2
set nofoldenable
" Turn-on case insensitive matches, but only when the pattern is all lowercase
set ignorecase
set smartcase

"--------------------------------------------------------------------------
" Specific Win32 Settings
"--------------------------------------------------------------------------

if has("win32")
	set clipboard=unnamed           " Use system clipboard
    set tabstop=4                   " Use 4 spaces for tabs
endif


"--------------------------------------------------------------------------
" Auto commands
"--------------------------------------------------------------------------

" Enable file type detection.
filetype plugin indent on

" Clear traling space before save (May cause problems generating diffs)
" autocmd BufWrite * mark z | %s/\s\+$//e | 'z

" switch to current dir
autocmd BufEnter * :sil! lcd %:p:h

" Executes pylint when python file is saved
"autocmd FileType python compiler pylint

" Executes flake8 when python file is saved
"autocmd FileType python compiler flake8

" Executes npm run build for make
autocmd Filetype javascript setlocal makeprg=npm\ run\ build

" Get the 2-space YAML as the default when hit carriage return after the colon
autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab

"--------------------------------------------------------------------------
" Keyboard mappings
"--------------------------------------------------------------------------

" Middle mouse button paste text from the clipboard
map! <S-Insert> <MiddleMouse>
map  <S-Insert> <MiddleMouse>
" Save
map <C-s> :w<Enter>
" Save and make
map <M-m> :wa<Enter>:make
" Buffer navigation
map <C-n>  :bnext<Enter>
map <C-p>  :bprev<Enter>

"--------------------------------------------------------------------------
" Full screen
"--------------------------------------------------------------------------

if has("win32")
    nnoremap <M-f> :echo "Not implemented"<CR>
    inoremap <M-f> <C-\><C-O>:echo "Not implemented"<CR>
else
    function! ToggleFullScreen()
       call system("wmctrl -i -r ".v:windowid." -b toggle,fullscreen")
       redraw
    endfunction

    nnoremap <M-f> :call ToggleFullScreen()<CR>
    inoremap <M-f> <C-\><C-O>:call ToggleFullScreen()<CR>
endif

"--------------------------------------------------------------------------
" Install plugins via Vim-Plug
"--------------------------------------------------------------------------
"
call plug#begin('~/.vim/plugged')

Plug 'https://github.com/flazz/vim-colorschemes'
Plug 'https://github.com/scrooloose/nerdtree'
Plug 'https://github.com/jlanzarotta/bufexplorer'
Plug 'https://github.com/majutsushi/tagbar'
"Plug 'https://github.com/ap/vim-buftabline'
Plug 'https://github.com/fatih/vim-go'
Plug 'https://github.com/natebosch/vim-lsc'
Plug 'https://github.com/wahidrahim/resize-font'
Plug 'https://github.com/chrisbra/unicode.vim'
"Plug 'https://github.com/vim-syntastic/syntastic'
"Plug 'https://github.com/prabirshrestha/async.vim'
"Plug 'https://github.com/prabirshrestha/vim-lsp'
"Plug 'autozimu/LanguageClient-neovim', {
"    \ 'branch': 'next',
"    \ 'do': 'bash install.sh',
"    \ }

call plug#end()

"--------------------------------------------------------------------------
" Plugins configurations
"--------------------------------------------------------------------------
"
" NERDTree
let NERDTreeIgnore=['\.o$']
map <M-left>   :NERDTreeToggle<Enter>

" Tagbar
let g:tagbar_left = 1
map <M-right>  :TagbarToggle<Enter>

" bufexplorer
let g:bufExplorerDefaultHelp=0
let g:bufExplorerSplitBelow=0
let g:bufExplorerShowDirectories=1
let g:bufExplorerSortBy='name'
let g:bufExplorerUseCurrentWindow=1
map <C-b>      :BufExplorerHorizontalSplit<Enter>
map <M-up>     :BufExplorerHorizontalSplit<Enter>


" Buftabline
let g:buftabline_numbers=2
let g:buftabline_indicators=1
nmap <leader>1 <Plug>BufTabLine.Go(1)
nmap <leader>2 <Plug>BufTabLine.Go(2)
nmap <leader>3 <Plug>BufTabLine.Go(3)
nmap <leader>4 <Plug>BufTabLine.Go(4)
nmap <leader>5 <Plug>BufTabLine.Go(5)
nmap <leader>6 <Plug>BufTabLine.Go(6)
nmap <leader>7 <Plug>BufTabLine.Go(7)
nmap <leader>8 <Plug>BufTabLine.Go(8)
nmap <leader>9 <Plug>BufTabLine.Go(9)
nmap <leader>0 <Plug>BufTabLine.Go(10)

" vim-lsc
let g:lsc_auto_map = v:false
let g:lsc_server_commands = {
    \ 'cpp': {
    \       'name':     'clangd',
    \       'command':  'clangd -background-index',
    \       'suppress_stderr': v:true
    \  },
    \  "go": {
    \       "command": "gopls serve",
    \       "log_level": -1,
    \       "suppress_stderr": v:true,
    \  },
    \}
let g:lsc_auto_map = {
    \ 'GoToDefinition': 'gd',
    \ 'GoToDefinitionSplit': 'gds',
    \ 'FindReferences': 'gr',
    \ 'NextReference': 'gn',
    \ 'PreviousReference': 'gp',
    \ 'FindImplementations': 'gi',
    \ 'FindCodeActions': 'ga',
    \ 'Rename': 'gR',
    \ 'ShowHover': v:true,
    \ 'DocumentSymbol': 'go',
    \ 'WorkspaceSymbol': 'gs',
    \ 'SignatureHelp': 'gm',
    \ 'Completion': 'completefunc',
    \}

" vim-go
"let g:go_def_mode='gopls'
"let g:go_info_mode='gopls'

"" Syntastic
"set statusline+=%#warningmsg#
"set statusline+=%{SyntasticStatuslineFlag()}
"set statusline+=%*
"let g:syntastic_always_populate_loc_list = 1
"let g:syntastic_auto_loc_list = 1
"let g:syntastic_check_on_open = 1
"let g:syntastic_check_on_wq = 0
"let g:syntastic_cpp_checkers = ['clang-tidy']


"let g:LanguageClient_serverCommands = {
"    \ 'cpp': ['clangd'],
"    \ }
"set completefunc=LanguageClient#complete
"
"nnoremap <silent> K :call LanguageClient#textDocument_hover()<CR>
"nnoremap <silent> gd :call LanguageClient#textDocument_definition()<CR>
"nnoremap <silent> <F2> :call LanguageClient#textDocument_rename()<CR>

" Vim Lsp
"if executable('clangd')
"    au User lsp_setup call lsp#register_server({
"        \ 'name': 'clangd',
"        \ 'cmd': {server_info->['clangd', '-background-index']},
"        \ 'whitelist': ['c', 'cpp', 'objc', 'objcpp'],
"        \ })
"endif

" resize font plugin key mapping
nmap <M-=>     :call ResizeFontBigger()<Enter>
nmap <M-->     :call ResizeFontSmaller()<Enter>


