"------------------------------------------------------------------------------
" VIM Configuraton file (GUI VERSION)
"------------------------------------------------------------------------------
" Last modification: 2015/12/02
"--------------------------------------------------------------------------

"--------------------------------------------------------------------------
" Window position and size
"--------------------------------------------------------------------------

if has("win32")
    winpos 230 130
    set lines=58
    set columns=120
else
    winpos 0 0
    set lines=66
    set columns=140
endif

"--------------------------------------------------------------------------
" Font
"--------------------------------------------------------------------------

if has("win32")
    set guifont=Consolas:h13:cANSI
else
	set guifont=DejaVu\ Sans\ Mono\ 13
endif

"--------------------------------------------------------------------------
" Preferences
"--------------------------------------------------------------------------

set guioptions-=T           " No Toolbar
set guicursor=a:blinkon0    " No blinking cursor
set guioptions-=L           " No left hand scrollbars
set guioptions-=m           " No menu bar
set t_vb=                   " Turn off bell flashes
if !has("win32")
    set noshowcmd
endif

"--------------------------------------------------------------------------
" Printer options
"--------------------------------------------------------------------------

set printoptions+=left:5pc

"--------------------------------------------------------------------------
" Color scheme
"--------------------------------------------------------------------------

	colorscheme moria
"	colorscheme desert
"	colorscheme desertEx
"	colorscheme metacosm
highlight StatusLine gui=none
highlight SpecialKey guibg=bg
highlight Folded     gui=none
highlight Boolean    gui=none

